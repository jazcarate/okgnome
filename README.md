# OkGnme
A fun rift on "okCupid" and "happn"

## Development
This proyect is bootstraped with [expo](https://expo.io/) (React Native).

### Instalation
1. Have node version `12.13.0` (latest LST). _pro tip: use [`nvm`](https://github.com/nvm-sh/nvm)
1. Have [`yarn`](https://yarnpkg.com/) installed.
1. Run `$ yarn` to install deps
1. run `$ yarn web` to run expo in the web. (if you have an iOS or android emulator / phone conected, you might want to `$ yarn ios` or `yarn android` insted)
