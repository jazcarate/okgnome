module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "jest": true,
    },
    "extends": [
        "airbnb"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly",
        "__DEV__": true
    },
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "settings": {
        "import/resolver": {
            "babel-module": {
                "extensions": [".ts", ".tsx"]
            }
        }
    },
    "plugins": [
        "react",
        "@typescript-eslint"
    ],
    "rules": {
        "import/no-extraneous-dependencies": "off",
        "no-use-before-define": ["error", { "variables": false }],
        "react/prop-types": "off",
        "no-unused-vars": "off",
        "react/jsx-filename-extension": ["error", { "extensions": [".tsx"] }],
        "import/no-unresolved": ["error", { "ignore": ["react-native", "expo-linear-gradient"] }],
        "max-len": "warn",
        "camelcase": "off",
    }
};