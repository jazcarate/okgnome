# Screens
How the app _flows_.

- `Main` **switch** that starts the whole app.
  - `Spash` **screen** (that is only shown in the first run).
  - `Profiles` **stack** the handles both the profiles and the current party.
    - `Details` **screen** to look at an individual profile 
    - `Home` **tabs**
      - `Party` **screen** to see the current party
      - `Swiper` **screen** to look at all the gnomes