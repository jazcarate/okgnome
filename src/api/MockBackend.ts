import { Backend, Locations } from './Backend';

function wait(seconds: number) {
  return new Promise((res) => setTimeout(res, seconds));
}

const MockBackend: Backend = {
  async getProfiles(): Promise<Locations> {
    await wait(4000);
    return require('~/api/data.json'); // eslint-disable-line global-require
  },
};

export default MockBackend;
