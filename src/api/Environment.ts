import RealBackend from '~/api/RealBackend';
import MockBackend from '~/api/MockBackend';
import { Backend } from '~/api/Backend';

type Environment = {
    backend: Backend,
};

const dev: Environment = {
  backend: MockBackend,
};

const prod: Environment = {
  backend: RealBackend,
};

export default __DEV__ ? dev : prod;
