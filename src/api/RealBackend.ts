import { Backend, Locations } from './Backend';

const RealBackend: Backend = {
  async getProfiles(): Promise<Locations> {
    const rawGnomes = await fetch('https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json');
    const gnomes = await rawGnomes.json();

    return gnomes;
  },
};

export default RealBackend;
