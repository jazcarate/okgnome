import Environment from '~/api/Environment';
import normalize from '~/api/normalize';
import { shuffle } from 'lodash';

const GnomeService = {
  async getGnomes() {
    const locations = await Environment.backend.getProfiles();
    const brastlewarkGnomes = locations.Brastlewark;

    return shuffle(normalize(brastlewarkGnomes));
  },
};

export default GnomeService;
