import { Gnome } from '~/api/Backend';

import professions from './professions';

export type Normalizator<T> = (a: T) => T;

const individual = (normalizator: Normalizator<Gnome>) => (gnomes: Gnome[]) => gnomes.map(normalizator);

const normalize = individual(professions);

export default normalize;
