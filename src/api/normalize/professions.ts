import { Gnome } from '../Backend';
import { Normalizator } from '.';

const professions: Normalizator<Gnome> = (gnome) => ({
  ...gnome,
  professions: gnome.professions.map((profession) => profession.trim()),
});

export default professions;
