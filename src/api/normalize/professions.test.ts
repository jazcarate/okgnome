import { aGnome } from 'test/profilesFixture';
import normalize from './professions';

describe('professionsNormalizator', () => {
  describe('when there is a gnome', () => {
    const gnome = aGnome({
      professions: [
        'Metalworker',
        'Woodcarver',
        'Stonecarver',
        ' Tinker',
        'Tailor',
        'Potter',
      ],
    });

    const normalizedGnome = normalize(gnome);
    it.each(normalizedGnome.professions)('%s does not have a leading space', (profession) => {
      expect(profession).toEqual(expect.not.stringMatching(/^ /));
    });
  });
});
