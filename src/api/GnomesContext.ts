import { createContext } from 'react';

import GnomeService from '~/api/GnomeService';

import wrapPromise from './wrapPromiseSuspense';

const gnomes = wrapPromise(
  GnomeService.getGnomes(),
);

const GnomesContext = createContext({
  gnomes,
});

export default GnomesContext;
