export type URL = string;
export type HairColor = string;
export type Name = string;
export type Profession = string;

export type Locations = {
    [location: string]: Gnome[],
}

export type Gnome = {
    id: number,
    name: Name,
    thumbnail: URL,
    age: number,
    weight: number,
    height: number,
    hair_color: HairColor,
    professions: Profession[],
    friends: Name[],
};

export interface Backend {
    getProfiles(): Promise<Locations>;
}
