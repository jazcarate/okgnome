import React, { FunctionComponent } from 'react';
import {
  SafeAreaView, Text, StyleSheet, View,
} from 'react-native';
import Constants from 'expo-constants';
import { NavigationBottomTabScreenComponent } from 'react-navigation-tabs';
import { Ionicons } from '@expo/vector-icons';
import { colors } from '~/components/Styles';

const Author: FunctionComponent = ({ children }) => (
  <Text style={styles.author}>
    {children}
  </Text>
);

const URL: FunctionComponent = ({ children }) => (
  <Text style={styles.url}>
    {children}
  </Text>
);

const About: NavigationBottomTabScreenComponent = () => (
  <>
    <SafeAreaView style={styles.container}>
      <Text>This is a coding challenge app.</Text>
      <Text>
I did not make any of the profesions Icons.
        They were made by these awesome people:
      </Text>
      <View style={styles.attribution}>
        <Text>
by
          <Author>Smashicons</Author>
        </Text>
        <Text>
by
          <Author>ultimatearm</Author>
        </Text>
        <Text>
by
          <Author>Freepik</Author>
        </Text>
        <Text>
by
          <Author>wanicon</Author>
        </Text>
        <Text>
by
          <Author>Pause08</Author>
        </Text>
        <Text>
by
          <Author>Good Ware</Author>
        </Text>
        <Text>
by
          <Author>Dimitry Miroliubov</Author>
        </Text>
        <Text>
by
          <Author>surang</Author>
        </Text>
        <Text>
from
          <URL>www.flaticon.com</URL>
        </Text>
      </View>
    </SafeAreaView>
  </>
);


About.navigationOptions = () => ({
  tabBarIcon: ({ tintColor }) => <Ionicons name="md-help-circle-outline" color={tintColor} size={32} />,
});

export default About;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: '10%',
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    justifyContent: 'center',
  },
  attribution: {
    lineHeight: 3,
    paddingTop: 30,
    paddingHorizontal: 50,
    color: colors.normal,
  },
  url: {
    fontWeight: 'bold',
    textDecorationLine: 'underline',
    color: colors.cta,
  },
  author: {
    fontWeight: 'bold',
  },
});
