import React, {
  useContext, FunctionComponent, useEffect, useState,
} from 'react';
import {
  Text, StyleSheet, View, ImageBackground,
} from 'react-native';
import Animated from 'react-native-reanimated';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationStackScreenComponent } from 'react-navigation-stack';

import GnomesContext from '~/api/GnomesContext';
import DetailsLink from '~/components/DetailsLink';
import Avatar from '~/components/Avatar';
import ProfessionIcon from '~/components/ProfessionIcon';

import { colors } from '~/components/Styles';
import { getGender, genderColor } from '~/domain/gender';

const Divider: FunctionComponent = () => (
  <View style={{
    borderWidth: 0.5,
    borderColor: colors.normal,
    margin: 10,
  }}
  />
);

const TwoColumns: FunctionComponent<{ children: JSX.Element[] }> = ({ children }) => (
  <View style={{
    flex: 1, flexDirection: 'row', alignContent: 'stretch', flexWrap: 'wrap',
  }}
  >

    {children.map((child, i) => ( // eslint-disable-next-line react/no-array-index-key
      <View key={`stat-${child.key}-${i}`} style={{ width: '50%' }}>
        {child}
      </View>
    ))}
  </View>
);

const Header: FunctionComponent = ({ children }) => <Text style={styles.header}>{children}</Text>;

const Details: NavigationStackScreenComponent = ({ navigation }) => {
  const backend = useContext(GnomesContext);
  const thisName = navigation.getParam('name', null);

  const [opacity] = useState(new Animated.Value(0));
  useEffect(Animated.spring(opacity, {
    ...Animated.SpringUtils.makeDefaultConfig(),
    toValue: 1,
  }).start, [navigation]);

  const profile = backend.gnomes.read().find(({ name }) => name === thisName);
  const {
    thumbnail, professions, friends, age, name, weight, height, hair_color: hairColor,
  } = profile;

  return (
    <>
      <ScrollView style={styles.container}>
        <View style={styles.top}>
          <Avatar source={{ uri: thumbnail }} size={150} borderColor={genderColor(profile)} />
        </View>
        <Text style={{ fontSize: 30, color: colors.highlight }}>
          {name}
          ,
          {' '}
          {age}
        </Text>

        <Divider />

        <Header>Who am I?</Header>
        <TwoColumns>
          <Animated.Text style={{ opacity }}>
            Gender:
            {getGender(profile)}
          </Animated.Text>
          <Animated.Text style={{ opacity }}>
            Hair color:
            {hairColor}
          </Animated.Text>
          <Animated.Text style={{ opacity }}>
            Weight:
            {Math.round(weight)}
          </Animated.Text>
          <Animated.Text style={{ opacity }}>
            Height:
            {Math.round(height)}
          </Animated.Text>
        </TwoColumns>

        <Divider />

        <Header>Professions</Header>
        {professions.length > 0
          ? (
            <TwoColumns>
              {professions.map((profesion) => (
                <View
                  key={`prof-${profesion}`}
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                  }}
                >
                  <ProfessionIcon profession={profesion} size={15} />
                  <Animated.Text style={{ opacity }}>{profesion}</Animated.Text>
                </View>
              ))}
            </TwoColumns>
          )
          : <Text style={{ fontStyle: 'italic' }}>We are all trying to find our purpose in life.</Text>}

        <Divider />

        <Header>Friends</Header>
        <View>
          {friends.length > 0
            ? friends.map((friend) => (
              <DetailsLink
                key={`friend-${friend}`}
                name={friend}
                goToDetails={() => navigation.push('Details', { name: friend })}
              />
            ))
            : <Text style={{ fontStyle: 'italic' }}>Looking to meet the right kind of friends.</Text>}
        </View>
      </ScrollView>
    </>
  );
};

export default Details;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 40,
  },
  top: {
    alignItems: 'center',
    marginTop: 10,
  },
  header: { fontSize: 20, color: colors.highlight, marginBottom: 10 },
});
