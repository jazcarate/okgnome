import React, { FunctionComponent, useContext, useEffect } from 'react';

import Constants from 'expo-constants';
import {
  StyleSheet, Button, AsyncStorage, View, Text,
} from 'react-native';
import { SafeAreaView, NavigationSwitchScreenProps } from 'react-navigation';

import GnomesContext from '~/api/GnomesContext';
import { colors } from '~/components/Styles';

const SHOULD_SKIP_SPLASH_KEY = 'skipSplash';

const SplashScreen: FunctionComponent<NavigationSwitchScreenProps> = ({ navigation }) => {
  useContext(GnomesContext); // Start loading, but don't block

  const skipSplash = () => {
    AsyncStorage.setItem(SHOULD_SKIP_SPLASH_KEY, 'true');
    navigation.navigate('Profiles');
  };

  const shouldSkip = async () => {
    try {
      const value = await AsyncStorage.getItem(SHOULD_SKIP_SPLASH_KEY);
      if (value) {
        skipSplash();
      }
    } catch (error) {
      // If it all fails. Don't skip
    }
  };

  useEffect(() => {
    shouldSkip();
  }, []);

  return (
    <>
      <SafeAreaView style={styles.container}>
        <Text style={styles.impact}>
          Welcome to Brastlewark!
        </Text>

        <Text style={styles.drop}>
          Are you ready to find the best party?
        </Text>

        <View style={styles.button}>
          <Button color={colors.cta} title="I'm Ready!" onPress={skipSplash} />
        </View>
      </SafeAreaView>
    </>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    height: '100%',
    marginTop: Constants.statusBarHeight,
    paddingVertical: 50,
    backgroundColor: 'transparent',
  },
  impact: {
    fontSize: 30,
  },
  drop: {
    fontSize: 20,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});
