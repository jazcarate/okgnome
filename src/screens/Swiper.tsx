import React, { useContext, Suspense } from 'react';
import { SafeAreaView, StyleSheet, ActivityIndicator } from 'react-native';

import { NavigationBottomTabScreenComponent } from 'react-navigation-tabs';
import { Ionicons } from '@expo/vector-icons';
import Swiper from '~/components/Swiper';
import GnomesContext from '~/api/GnomesContext';
import { HomeScreenProps } from './Home';
import { Name } from '~/api/Backend';

const Swipper: NavigationBottomTabScreenComponent<{}, HomeScreenProps> = ({ navigation, screenProps: { addToParty } }) => {
  const backend = useContext(GnomesContext);

  return (
    <>
      <SafeAreaView style={styles.container}>
        <Suspense fallback={<ActivityIndicator size="large" color="#0000ff" />}>
          <Swiper
            profiles={backend.gnomes}
            onRight={addToParty}
            goToDetails={(name: Name) => navigation.navigate('Details', { name })}
          />
        </Suspense>
      </SafeAreaView>
    </>
  );
};

Swipper.navigationOptions = () => ({
  tabBarIcon: ({ tintColor }) => <Ionicons name="md-add-circle" color={tintColor} size={32} />,
});

export default Swipper;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
});
