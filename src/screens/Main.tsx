import { createSwitchNavigator, createAppContainer } from 'react-navigation';

import Splash from '~/screens/Splash';
import Profiles from '~/screens/Profiles';

const Main = createSwitchNavigator(
  {
    Splash,
    Profiles,
  },
  {
    initialRouteName: 'Splash',
  },
);

export default Main;
