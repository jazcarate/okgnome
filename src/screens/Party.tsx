import React, {
  FunctionComponent, useState, Dispatch, SetStateAction,
} from 'react';

import {
  SafeAreaView, FlatList, View, Text, StyleSheet, TouchableOpacity,
} from 'react-native';
import { NavigationBottomTabScreenComponent } from 'react-navigation-tabs';
import { withNavigation, NavigationSwitchScreenProps } from 'react-navigation';
import { ScrollView } from 'react-native-gesture-handler';
import Constants from 'expo-constants';
import { uniq } from 'lodash';

import { HomeScreenProps } from '~/screens/Home';
import PartyIcon from '~/components/PartyIcon';
import { colors } from '~/components/Styles';
import PartyMember, { PartyRow } from '~/components/PartyMember';
import { Gnome, Profession } from '~/api/Backend';
import FilterPill from '~/components/FilterPill';
import shouldShow from '~/domain/filters';

const Header: FunctionComponent<{
  party: Gnome[],
  filters: Profession[],
  setFilters: Dispatch<SetStateAction<Profession[]>>
}> = ({ party, filters, setFilters }) => (
  <View style={{ backgroundColor: 'white' }}>
    <Text style={{
      fontWeight: 'bold', fontSize: 40, color: colors.normal, flex: 1, textAlign: 'center',
    }}
    >
      Your Party
    </Text>
    <ScrollView horizontal style={{ padding: 10 }}>
      {uniq(party.flatMap((_) => _.professions)).sort().map((profession) => (
        <FilterPill
          key={profession}
          filters={filters}
          setFilters={setFilters}
          profession={profession}
        />
      ))}
    </ScrollView>
  </View>
);

const LFG: FunctionComponent<NavigationSwitchScreenProps> = ({ navigation }) => (
  <PartyRow
    onPress={() => navigation.navigate('Swiper')}
    text="Single human LFG"
    source={require('assets/images/placeholderGnome.jpg')} // eslint-disable-line global-require
  />
);

const Party: NavigationBottomTabScreenComponent<{}, HomeScreenProps> = ({ navigation, screenProps: { party, kickFromParty } }) => {
  const [filters, setFilters] = useState([] as Profession[]);

  return (
    <>
      <SafeAreaView style={styles.container}>
        <FlatList
          ListHeaderComponent={(
            <Header
              party={party}
              filters={filters}
              setFilters={setFilters}
            />
          )}
          ListEmptyComponent={withNavigation(LFG)}
          data={party}
          renderItem={({ item }) => (
            <PartyMember
              key={item.id}
              member={item}
              show={shouldShow(item, filters)}
              goToDetails={() => navigation.navigate('Details', { name: item.name })}
              kickFromParty={() => kickFromParty(item)}
            />
          )}
          keyExtractor={(_, i) => String(i)}
          stickyHeaderIndices={[0]}
        />
      </SafeAreaView>
    </>
  );
};

Party.navigationOptions = ({ screenProps: { party } }) => ({
  tabBarIcon: ({ tintColor }) => (
    <PartyIcon
      partyCount={party.length}
      color={tintColor}
      size={32}
    />
  ),
});

export default Party;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
  },
});
