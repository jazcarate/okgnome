import React, { FunctionComponent } from 'react';
import { NavigationRouter } from 'react-navigation';
import { NavigationTabProp } from 'react-navigation-tabs';

import Navigator from './Navigator';
import { Gnome } from '~/api/Backend';
import useParty from '~/components/useParty';

type Props = {
  navigation: NavigationTabProp<{}>;
};

type RoutedComponent<T> = FunctionComponent<T> & { router: NavigationRouter };

export type HomeScreenProps = {
  party: Gnome[],
  addToParty: (x: Gnome) => void,
  kickFromParty: (x: Gnome) => void,
};

const Profiles: RoutedComponent<Props> = ({ navigation }) => {
  const [party, addToParty, kickFromParty] = useParty();

  return (
    <Navigator
      navigation={navigation}
      screenProps={{
        party,
        addToParty,
        kickFromParty,
      }}
    />
  );
};


Profiles.router = Navigator.router;

export default Profiles;
