
import { createBottomTabNavigator } from 'react-navigation-tabs';

import { createAppContainer } from 'react-navigation';
import Party from '../Party';
import Swiper from '../Swiper';
import About from '~/screens/About';
import { colors } from '~/components/Styles';

const ProfilesNavigator = createBottomTabNavigator({
  Swiper,
  Party,
  About,
}, {
  initialRouteName: 'Swiper',
  tabBarOptions: {
    activeTintColor: colors.highlight,
    inactiveTintColor: colors.normal,
  },
});

export default createAppContainer(ProfilesNavigator);
