import { createStackNavigator } from 'react-navigation-stack';

import Details from '~/screens/Details';
import Home from '~/screens/Home';

const Profiles = createStackNavigator({
  Details: {
    screen: Details,
    navigationOptions: ({ navigation }) => ({
      headerTitle: navigation.getParam('name', null),
    }),
  },
  Home: {
    screen: Home,
    navigationOptions: () => ({
      headerShown: false,
    }),
  },
}, {
  initialRouteName: 'Home',
  transparentCard: true,
});

export default Profiles;
