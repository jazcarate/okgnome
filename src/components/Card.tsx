import React, { useState, FunctionComponent } from 'react';
import {
  StyleSheet, Dimensions, Text, Image, View, TouchableOpacity,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

import { Gnome, Name } from '~/api/Backend';
import ProfessionIcon from '~/components/ProfessionIcon';
import { colors } from '~/components/Styles';
import { genderColor } from '~/domain/gender';

type Props = {
  profile: Gnome,
  goToDetails: (x: Name) => void,
};

const Card: FunctionComponent<Props> = ({ profile, goToDetails }) => {
  const {
    thumbnail, professions, name, age,
  } = profile;

  const genderStyle = {
    borderColor: genderColor(profile),
    borderWidth: 10,
  };

  return (
    <View style={StyleSheet.flatten([styles.container, genderStyle])}>
      <Image
        style={styles.imageContainer}
        source={{ uri: thumbnail }}
        defaultSource={require('../../assets/images/placeholderGnome.jpg')} // eslint-disable-line global-require
        resizeMode="cover"
      />

      <LinearGradient
        colors={['rgba(255,255,255,0.3)', 'rgba(255,255,255,1)']}
        style={styles.details}
      >
        <View style={StyleSheet.flatten([styles.flex])}>
          <Text numberOfLines={1} style={styles.name}>
            {name}
            ,
            {' '}
            {age}
          </Text>
        </View>
        <View style={StyleSheet.flatten([styles.flex, styles.lowerLine])}>
          <View style={StyleSheet.flatten([styles.flex, styles.professions])}>
            {professions.slice(0, 3).map((profession) => <ProfessionIcon key={`prof-${profession}`} profession={profession} />)}
          </View>
          <View>
            <TouchableOpacity onPress={() => goToDetails(name)}>
              <Ionicons name="md-information-circle-outline" size={50} color={colors.cta} />
            </TouchableOpacity>
          </View>
        </View>
      </LinearGradient>
    </View>
  );
};

export default Card;

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    width: Dimensions.get('window').width - 30,
    height: Dimensions.get('window').height - 80,
    borderRadius: 20,
    overflow: 'hidden',
  },
  imageContainer: {
    flex: 2,
    backgroundColor: 'gray',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
  details: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    alignItems: 'flex-start',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    width: '100%',
    height: '20%',
    padding: 10,
  },
  name: {
    fontSize: 25,
    color: 'white',
    textShadowColor: 'black',
    textShadowOffset: { width: 0, height: 0 },
    textShadowRadius: 4,
  },
  professions: {
    flex: 1,
    flexDirection: 'row',
  },
  lowerLine: {
    flex: 1,
    flexDirection: 'row',
    alignContent: 'space-between',
    width: '100%',
  },
});
