import { StyleSheet } from 'react-native';

export const colors = {
  normal: '#845EC2',
  highlight: '#00C9A7',
  cta: '#008F7A',
};

export default StyleSheet.create({
  text: {
    color: colors.normal,
  },
  textHighlight: {
    color: colors.highlight,
  },
  cta: {
    color: colors.cta,
    fontWeight: 'bold',
  },
});
