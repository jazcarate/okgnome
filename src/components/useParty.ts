import { useState } from 'react';

import { Gnome } from '~/api/Backend';
import { add, remove } from '~/domain/lists';

export default function useParty() {
  const [party, setParty] = useState([] as Gnome[]);

  const addToParty = (newPartyMember: Gnome) => {
    setParty((oldParty) => add(oldParty, newPartyMember));
  };

  const kickFromParty = (memberToKick: Gnome) => {
    setParty((oldParty) => remove(oldParty, memberToKick));
  };

  return [party, addToParty, kickFromParty];
}
