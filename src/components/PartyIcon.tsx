import React, { FunctionComponent, Suspense } from 'react';
import { View, Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { colors } from './Styles';

type Props = {
    partyCount?: number,
    color: string,
    size?: number,
}

const PartyIcon: FunctionComponent<Props> = ({ partyCount = 0, color = 'black', size = 32 }) => (
  <View style={{ width: size, height: size }}>
    <Ionicons name="md-people" color={color} size={size} />
    {partyCount > 0 && (
    <View
      style={{
        // If you're using react-native < 0.57 overflow outside of parent
        // will not work on Android, see https://git.io/fhLJ8
        position: 'absolute',
        right: -6,
        top: -3,
        backgroundColor: colors.cta,
        borderRadius: 6,
        width: 12,
        height: 12,
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Text style={{ color: 'white', fontSize: 10, fontWeight: 'bold' }}>
        {partyCount}
      </Text>
    </View>
    )}
  </View>
);

export default PartyIcon;
