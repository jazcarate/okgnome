import React, { FunctionComponent, Dispatch, SetStateAction } from 'react';

import { View, StyleSheet, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Ionicons } from '@expo/vector-icons';

import { colors } from './Styles';
import { Profession } from '~/api/Backend';
import { add, remove } from '~/domain/lists';

const FilterPill: FunctionComponent<{
  profession: Profession,
  filters: Profession[],
  setFilters: Dispatch<SetStateAction<Profession[]>>
}> = ({ profession, filters, setFilters }) => {
  const enabled = filters.includes(profession);

  const toggleFilter = () => {
    if (enabled) {
      setFilters((oldFilters) => remove(oldFilters, profession));
    } else {
      setFilters((oldFilters) => add(oldFilters, profession));
    }
  };

  return (
    <TouchableOpacity onPress={toggleFilter} activeOpacity={0.8}>
      <View style={StyleSheet.flatten([
        styles.pill,
        enabled ? styles.enabledPill : styles.disabledPill,
      ])}
      >
        <View style={styles.contentContainer}>
          <Text style={{ color: 'white' }} numberOfLines={1}>
            {profession}
          </Text>
        </View>
        <View style={styles.actionContainer}>
          <Ionicons
            style={styles.actionIcon}
            name={enabled
              ? 'md-checkmark-circle'
              : 'md-close-circle'}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default FilterPill;

const styles = StyleSheet.create({
  enabledPill: {
    backgroundColor: colors.highlight,
  },
  disabledPill: {
    backgroundColor: colors.normal,
  },
  pill: {
    height: 40,
    borderRadius: 40,
    padding: 5,
    marginHorizontal: 5,
    alignSelf: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    marginRight: 15,
  },
  actionContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f5f5f5',
    width: 20,
    height: 20,
    borderRadius: 20,
  },
  actionIcon: {
    width: 15,
    height: 15,
  },
});
