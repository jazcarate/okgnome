import React, { FunctionComponent } from 'react';
import { Text, TouchableOpacity } from 'react-native';

import { Name } from '~/api/Backend';

const DetailsLink: FunctionComponent<{
    name: Name;
    goToDetails: () => void;
}> = ({ name, goToDetails }) => (
  <TouchableOpacity onPress={goToDetails}>
    <Text style={{ fontWeight: 'bold' }}>{name}</Text>
  </TouchableOpacity>
);

export default DetailsLink;
