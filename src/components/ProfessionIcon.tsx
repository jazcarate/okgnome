import React, { FunctionComponent } from 'react';

import { Image } from 'react-native';

import { Profession } from '~/api/Backend';

type Props = {
    profession: Profession,
    size?: number,
}

/* eslint-disable global-require */
export const professionsAssets = {
  Metalworker: require('assets/images/profesions/Metalworker.png'),
  Woodcarver: require('assets/images/profesions/WoodCarver.png'),
  Stonecarver: require('assets/images/profesions/Stonecarver.png'),
  Tinker: require('assets/images/profesions/Tinker.png'),
  Tailor: require('assets/images/profesions/Tailor.png'),
  Potter: require('assets/images/profesions/Potter.png'),
  Brewer: require('assets/images/profesions/Brewer.png'),
  Medic: require('assets/images/profesions/Medic.png'),
  Prospector: require('assets/images/profesions/Prospector.png'),
  Gemcutter: require('assets/images/profesions/Gemcutter.png'),
  Mason: require('assets/images/profesions/Mason.png'),
  Cook: require('assets/images/profesions/Cook.png'),
  Baker: require('assets/images/profesions/Baker.png'),
  Miner: require('assets/images/profesions/Miner.png'),
  Carpenter: require('assets/images/profesions/Carpenter.png'),
  Farmer: require('assets/images/profesions/Farmer.png'),
  'Tax inspector': require('assets/images/profesions/TaxInspector.png'),
  Smelter: require('assets/images/profesions/Smelter.png'),
  Butcher: require('assets/images/profesions/Butcher.png'),
  Sculptor: require('assets/images/profesions/Sculptor.png'),
  Blacksmith: require('assets/images/profesions/Blacksmith.png'),
  Mechanic: require('assets/images/profesions/Mechanic.png'),
  Leatherworker: require('assets/images/profesions/Leatherworker.png'),
  'Marble Carver': require('assets/images/profesions/MarbleCarver.png'),
};
/* eslint-enable  global-require */

const ProfessionIcon: FunctionComponent<Props> = ({ profession, size = 50 }) => (
  <Image
    source={professionsAssets[profession]}
    style={{ width: size, height: size, marginRight: size / 5 }}
  />
);

export default ProfessionIcon;
