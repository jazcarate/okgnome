import React, { FunctionComponent } from 'react';
import DeckSwiper from 'react-native-deck-swiper';
import Card from './Card';
import { Suspensable } from '~/api/wrapPromiseSuspense';
import { Gnome, Name } from '~/api/Backend';


type Props = {
  profiles: Suspensable<Gnome[]>;
  onLeft?: (a: Gnome) => void,
  onRight?: (a: Gnome) => void,
  goToDetails: (x: Name) => void,
};

const overlay = {
  left: {
    title: 'NOPE',
    style: {
      label: {
        backgroundColor: 'black',
        borderColor: 'black',
        color: 'white',
        borderWidth: 1,
      },
      wrapper: {
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
        marginTop: 30,
        marginLeft: -30,
      },
    },
  },
  right: {
    title: 'PARTY',
    style: {
      label: {
        backgroundColor: 'black',
        borderColor: 'black',
        color: 'white',
        borderWidth: 1,
      },
      wrapper: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        marginTop: 30,
        marginLeft: 30,
      },
    },
  },
};

const Swiper: FunctionComponent<Props> = ({
  profiles: suspensableProfiles, onLeft, onRight, goToDetails,
}) => {
  const profiles = suspensableProfiles.read();

  return (
    <DeckSwiper
      backgroundColor="transparent"
      keyExtractor={({ id }: Gnome) => id}
      onSwipedLeft={(profileIndex: number) => onLeft && onLeft(profiles[profileIndex])}
      onSwipedRight={(profileIndex: number) => onRight && onRight(profiles[profileIndex])}
      verticalSwipe={false}
      cards={profiles}
      renderCard={(profile: Gnome) => <Card profile={profile} goToDetails={goToDetails} />}
      stackSize={2}
      overlayLabels={overlay}
    />
  );
};

export default Swiper;
