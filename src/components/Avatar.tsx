import React, { FunctionComponent } from 'react';
import {
  StyleSheet, Image, ImageSourcePropType, StyleProp, ImageStyle,
} from 'react-native';
import { colors } from './Styles';

type Props = {
  source: ImageSourcePropType,
  style?: StyleProp<ImageStyle>,
  size?: number,
  borderColor?: string,
}

function sizeDependantImageStyle(size: number): StyleProp<ImageStyle> {
  return {
    width: size,
    height: size,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: size,
    borderBottomRightRadius: size,
    overflow: 'hidden',
    borderWidth: size / 50,
  };
}

const Avatar: FunctionComponent<Props> = ({
  source, style: imageStyle, size = 150, borderColor = colors.highlight,
}) => (
  <Image
    style={StyleSheet.flatten([imageStyle, sizeDependantImageStyle(size), { borderColor }])}
    source={source}
    defaultSource={require('../../assets/images/placeholderGnome.jpg')} // eslint-disable-line global-require
  />
);

export default Avatar;
