import React, { FunctionComponent, useState, useEffect } from 'react';

import {
  ImageSourcePropType, View, Dimensions, StyleSheet, Image, Text, TouchableOpacity,
} from 'react-native';
import Swipeout from 'react-native-swipeout';
import Animated from 'react-native-reanimated';

import { Gnome } from '~/api/Backend';
import { colors } from './Styles';

type PartyRowProp = {
    onPress: () => void,
    source: ImageSourcePropType,
    text: string,
}

const rowHeight = 170;

export const PartyRow: FunctionComponent<PartyRowProp> = ({ onPress, source, text }) => (
  <TouchableOpacity onPress={onPress}>
    <View style={styles.partyRow}>
      <Image
        style={styles.imageContainer}
        source={source}
        defaultSource={require('assets/images/placeholderGnome.jpg')} // eslint-disable-line global-require
        resizeMode="cover"
      />
      <Text numberOfLines={1} style={styles.title}>{text}</Text>
    </View>
  </TouchableOpacity>
);


type PartyMemberProp = {
    member: Gnome,
    show: boolean,
    goToDetails: () => void,
    kickFromParty: () => void
};

const PartyMember: FunctionComponent<PartyMemberProp> = ({
  show = true, member, goToDetails, kickFromParty,
}) => {
  const { name, thumbnail } = member;
  const [height] = useState(new Animated.Value(rowHeight));

  useEffect(Animated.spring(height, {
    ...Animated.SpringUtils.makeDefaultConfig(),
    overshootClamping: true,
    toValue: show ? rowHeight : 0,
  }).start, [show]);

  const swipeoutBtns = [
    {
      text: 'Kick',
      onPress: kickFromParty,
      backgroundColor: colors.highlight,
    },
  ];

  return (
    <Animated.View style={{ height }}>
      <Swipeout right={swipeoutBtns} style={styles.swipeout} autoClose>
        <PartyRow onPress={goToDetails} source={{ uri: thumbnail }} text={name} />
      </Swipeout>
    </Animated.View>
  );
};

export default PartyMember;

const styles = StyleSheet.create({
  partyItem: {
    flex: 1,
  },
  title: {
    fontSize: 32,
    color: 'white',
    position: 'absolute',
    left: 10,
    bottom: 20,
    padding: 3,
    textShadowColor: 'black',
    textShadowOffset: { width: 0, height: 0 },
    textShadowRadius: 4,
  },
  swipeout: {
    backgroundColor: 'transparent',
  },
  partyRow: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    width: Dimensions.get('window').width,
    height: rowHeight,
  },
  imageContainer: {
    flex: 2,
    backgroundColor: 'gray',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
});
