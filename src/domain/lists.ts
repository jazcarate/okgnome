import { uniq } from 'lodash';

export function add<T>(oldList: T[], thingToAdd: T): T[] {
  return uniq(oldList.concat(thingToAdd));
}

export function remove<T>(oldList: T[], thingToRemove: T): T[] {
  return oldList.filter((item) => item !== thingToRemove);
}
