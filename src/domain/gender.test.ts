import { aGnome } from 'test/profilesFixture';
import { getGender, genderColor } from './gender';

describe('getGender', () => {
  describe('when querying at the same profile', () => {
    it('has the same gender', () => {
      const aGender = getGender(aGnome());
      const anotherGender = getGender(aGnome());

      expect(aGender).toBe(anotherGender);
    });

    it('even when the name is uppercased', () => {
      const name = 'bob';
      const aGender = getGender(aGnome({ name }));
      const anotherGender = getGender(aGnome({ name: name.toUpperCase() }));

      expect(aGender).toBe(anotherGender);
    });
  });


  it('diferent names yeild different genders', () => {
    const aName = 'Bob';
    const anotherName = 'Alice';

    const aGender = getGender(aGnome({ name: aName }));
    const anotherGender = getGender(aGnome({ name: anotherName }));


    expect(aGender).not.toBe(anotherGender);
  });


  // I had an off by 1 error. This test is so that case is covered
  describe('previous bugs', () => {
    it('A gnome with "a" as the last letter of their first name has a gender', () => {
      const name = 'Libalia Tinkwidget';

      const aGender = getGender(aGnome({ name }));

      expect(aGender).not.toBeFalsy();
    });

    it('A gnome with "z" as the last letter of their first name has a gender', () => {
      const name = 'Cogwitz Tinkwidget';

      const aGender = getGender(aGnome({ name }));

      expect(aGender).not.toBeFalsy();
    });
  });
});

describe('genderColor', () => {
  describe('when passing a gnome', () => {
    it('a gnome has a gender color asociated with it', () => {
      expect(
        genderColor(aGnome()),
      ).toBe('#e132b6');
    });
  });

  describe('each gender has the specific color conforming to our data research', () => {
    it.each`
      gender              | color
      ${'Androgynous'}    | ${'#6a0bdc'}
      ${'Bigender'}       | ${'#64d2d4'}
      ${'Cis Female'}     | ${'#705851'}
      ${'Cis Male'}       | ${'#371ce6'}
      ${'FTM'}            | ${'#e9cf1b'}
      ${'Fluid'}          | ${'#7f85c0'}
      ${'Nonconforming'}  | ${'#d13312'}
      ${'Questioning'}    | ${'#859f13'}
      ${'Variant'}        | ${'#1e7316'}
      ${'MTF'}            | ${'#083a6d'}
      ${'Pangender'}      | ${'#75232e'}
      ${'Neither'}        | ${'#c298e1'}
      ${'Neutrois'}       | ${'#90583d'}
      ${'Trans Female'}   | ${'#404f47'}
      ${'Trans Male'}     | ${'#e132b6'}
      ${'Trans Person'}   | ${'#ecb966'}
      ${'Transfeminine'}  | ${'#62ad50'}
      ${'Transgender'}    | ${'#bf2a40'}
      ${'Transmasculine'} | ${'#c8da93'}
      ${'Two-Spirit'}     | ${'#a86fc4'}
  `('$gender color is $color)', ({ gender, color }) => {
  expect(
    genderColor(gender),
  ).toBe(color);
});
  });
});
