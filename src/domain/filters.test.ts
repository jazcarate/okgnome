import { aGnome } from 'test/profilesFixture';
import shouldShow from './filters';
import { Gnome, Profession } from '~/api/Backend';

describe('shouldShow', () => {
  let gnome: Gnome;
  let filters: Profession[];

  const subject = () => expect(shouldShow(gnome, filters));
  const expectShown = () => subject().toBeTruthy();
  const expectNowShown = () => subject().toBeFalsy();

  describe('a gnome with no professions', () => {
    beforeEach(() => {
      gnome = aGnome({ professions: [] });
    });

    describe('when the filter list is empty', () => {
      beforeEach(() => {
        filters = [];
      });

      it('does show', expectShown);
    });

    describe('when filtering for any profession', () => {
      beforeEach(() => {
        filters = ['tinker'];
      });

      it('does not show', expectNowShown);
    });
  });

  describe('a gnome with a profession', () => {
    const profession = 'Tinker';
    beforeEach(() => {
      gnome = aGnome({ professions: [profession] });
    });

    describe('when the filter list is empty', () => {
      beforeEach(() => {
        filters = [];
      });

      it('does show', expectShown);
    });

    describe('when filtering for their profession', () => {
      beforeEach(() => {
        filters = [profession];
      });

      it('does show', expectShown);
    });

    describe('when filtering for another profession', () => {
      beforeEach(() => {
        filters = ['Woodworker'];
      });

      it('does not show', expectNowShown);
    });
  });

  describe('a gnome with many professions', () => {
    const aProfession = 'Tinker';
    const anotherProfession = 'Woodworker';
    beforeEach(() => {
      gnome = aGnome({ professions: [aProfession, anotherProfession] });
    });

    describe('when the filter list is empty', () => {
      beforeEach(() => {
        filters = [];
      });

      it('does show', expectShown);
    });

    describe('when filtering for one of their profession', () => {
      beforeEach(() => {
        filters = [aProfession];
      });

      it('does show', expectShown);
    });

    describe('when filtering for all of their professions', () => {
      beforeEach(() => {
        filters = [aProfession, anotherProfession];
      });

      it('does show', expectShown);
    });

    describe('when filtering for another profession', () => {
      beforeEach(() => {
        filters = ['Gemcutter'];
      });

      it('does not show', expectNowShown);
    });
  });
});
