import { Gnome, Profession } from '~/api/Backend';

const shouldShow = ({ professions }: Gnome, filters: Profession[]) => filters.length === 0 || filters.every((profession) => professions.includes(profession));

export default shouldShow;
