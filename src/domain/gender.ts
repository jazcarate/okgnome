import { Gnome } from '~/api/Backend';

const gendersColor = {
  Androgynous: '#6a0bdc',
  Bigender: '#64d2d4',
  'Cis Female': '#705851',
  'Cis Male': '#371ce6',
  FTM: '#e9cf1b',
  Fluid: '#7f85c0',
  Nonconforming: '#d13312',
  Questioning: '#859f13',
  Variant: '#1e7316',
  MTF: '#083a6d',
  Pangender: '#75232e',
  Neither: '#c298e1',
  Neutrois: '#90583d',
  'Trans Female': '#404f47',
  'Trans Male': '#e132b6',
  'Trans Person': '#ecb966',
  Transfeminine: '#62ad50',
  Transgender: '#bf2a40',
  Transmasculine: '#c8da93',
  'Two-Spirit': '#a86fc4',
};

function letterToNumber(str: string): number {
  return str.charCodeAt(0);
}
const numberForA = letterToNumber('a');
const numberForZ = letterToNumber('z');

function lastLetterOfFirstName({ name }) {
  return name.split(' ')[0].slice(-1).toLowerCase();
}

export function getGender(gnome: Gnome) {
  const genders = Object.keys(gendersColor);

  const scale = ((letterToNumber(lastLetterOfFirstName(gnome)) - numberForA) / (numberForZ - numberForA)) * (genders.length - 1);

  return genders[Math.round(scale)];
}

export function genderColor(gender: keyof typeof gendersColor): string;
export function genderColor(gnome: Gnome): string;
export function genderColor(arg: keyof typeof gendersColor | Gnome): string {
  const gender = (typeof arg === 'string') ? arg : getGender(arg);

  return gendersColor[gender];
}
