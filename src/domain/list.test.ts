import { add, remove } from './lists';

describe('add', () => {
  const thingToAdd = 'Tinker';

  describe('when the list is empty', () => {
    const list = [];

    it('has the added thing', () => {
      expect(
        add(list, thingToAdd),
      ).toContain(thingToAdd);
    });
  });

  describe('when the list has the same thing to be added', () => {
    const list = [thingToAdd];

    it('does not change', () => {
      expect(
        add(list, thingToAdd),
      ).toEqual(list);
    });
  });

  describe('when the list has something', () => {
    const filters = ['Woodworker'];

    it('grows in length', () => {
      expect(
        add(filters, thingToAdd).length,
      ).toBe(filters.length + 1);
    });
  });
});

describe('remove', () => {
  const thingToRemove = 'Tinker';

  describe('when the list is empty', () => {
    const filtelists = [];

    it('is still empty', () => {
      expect(
        remove(filtelists, thingToRemove),
      ).toEqual(filtelists);
    });
  });

  describe('when the list has something', () => {
    const list = ['Woodworker'];

    it('does not change the original list', () => {
      expect(
        remove(list, thingToRemove),
      ).toEqual(list);
    });
  });

  describe('when the list has the item to remove', () => {
    const list = [thingToRemove];

    it('shrinks in length', () => {
      expect(
        remove(list, thingToRemove).length,
      ).toEqual(list.length - 1);
    });
  });
});
