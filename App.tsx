import React, { FunctionComponent, useEffect } from 'react';

import { createAppContainer } from 'react-navigation';
import { ImageBackground, StyleSheet } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import Main from '~/screens/Main';

const App = createAppContainer(Main);

export default () => {
    return (
        <ImageBackground
            source={require('assets/images/background.png')}
            imageStyle={{ resizeMode: 'repeat' }}
            style={styles.background}>
            <LinearGradient colors={['transparent', 'white']}
                style={{ flex: 1 }}>
                <App />
            </LinearGradient>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    background: {
        backgroundColor: 'white',
        width: '100%',
        height: '100%'
    }
})